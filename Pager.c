//Zach Pedersen, Justin Dietrich
//CST-315
//Prof. Citro
//This is our work!

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#define MAX 128

int main(){

int page[MAX],i,n,f,off,pno;
int choice=0;

printf("\nEnter number of pages in logical memory: ");
scanf("%d",&n);

printf("\nEnter number of frames: ");
scanf("%d",&f);

//Opens read file
FILE *reader;
reader = fopen("read.txt", "r");
if (reader == NULL) {
    printf("File not Found.");
    exit(1);
}

//Fills logical memory with strings from read file
char logical[n][MAX];
for (int i = 0; i < n; i++)
{
  fgets(logical[i], MAX, reader);
}
fclose(reader);

//User creates page table
printf("\nEnter the page table\n");
printf("(Enter frame number as -1 if that page is not present in any frame)\n\n");
printf("\npageno\tframeno\n-------\t-------");

for(i=0;i<n;i++){
printf("\n\n%d\t\t",i);
scanf("%d",&page[i]);
}

//Translates logical to physical memory
char physical [f][MAX];
int physicalPage [f];
for (int i = 0; i < f; i++)
{
  physicalPage[i] = -1;
}
for (int i = 0; i < n; i++)
{
  if (i > -1)
  {
    //physical[page[i]] = logical[i];
    strncpy(physical[page[i]], logical[i], MAX);
    physicalPage[page[i]] = i;
  }
}

//Writes logical memory to file
FILE *writer;
writer = fopen("program.txt", "w");
if (writer == NULL) {
    printf("File not Created.");
    exit(1);
}

//Prints Logical Memory Contents
fprintf(writer, "     Logical Memory:\n");
for (int i = 0; i < n; i++)
{
  fprintf(writer, "Page %i:\n%s\n", i, logical[i]);
}

//Prints Page Table
fprintf(writer, "\n\n     Page Table:\n");
for (int i = 0; i < n; i++)
{
  fprintf(writer, "%i | %i\n", i, page[i]);
}

//Prints Physical Memory Contents
fprintf(writer, "\n\n\n     Physical Memory:\n");
for (int i = 0; i < f; i++)
{
  fprintf(writer, "Frame %i:\n", i);
  if (physicalPage[i] == -1)
  {
    fprintf(writer, "----\n\n");
  }
  else
  {
    fprintf(writer, "%s(Page %i)\n\n", physical[i], physicalPage[i]);
  }
}
fclose(writer);

printf("\n\nNew file showing memory and page table created.\n");

return 1;

//fprintf(fptr, "%s", MAX);
}