Zach Pedersen
Justin Dietrich

Download both the Pager.c file and read.txt file. Make sure they are saved to the same directory.

In the linux terminal, navigate to the directory where the files are saved.

input the terminal commands:
gcc -o table Pager.c
./table

The program should be running now. It asks for logical and physical memory size.
The project suggests we use 4 and 8, in that order. Also there's only 6 words in the read.txt so higher numbers won't work.
Next you can create the page table however you want!
Just don't type any numbers out of the boundaries (0-7, if you did 8).

After that, a file called 'program.txt' will appear in the same directory as the program. It contains the output.